//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft shared
// source or premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license agreement,
// you are not authorized to use this source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the SOURCE.RTF on your install media or the root of your tools installation.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
#include <windows.h>
#include <Msgqueue.h>
#include "logging.h"
#include "ccamerastreamtest.h"
#include "camera.h"
#include "opencv_demo_thumb.h"

#define MSGQUEUE_WAITTIME 100
TCHAR g_tszClassName[] = TEXT("CamTest");
extern BOOL saveraw;
extern DWORD nover;
extern DWORD camType;
extern BOOL Singlechecked;
DWORD pat_r32[0x100];
DWORD pat_g32[0x100];
DWORD pat_b32[0x100];
float mmr, mmg, mmb;
DWORD halbe = 0;

extern int InformationArray[50];
extern HWND wregArray[25];
extern int runStream;

extern HWND wreg10;
extern HWND wregpopup;


DWORD halbecnt = 0;
DWORD oldto = 0;
extern BOOL resize_window;

unsigned char capture[752*480];

#undef ERRFAIL
// #define ERRFAIL(_x_) RETAILMSG(1,(_x_))
#define ERRFAIL(_x_) ;
#undef WARN
#define WARN(_x_) ; 

void StartImageProcessing(unsigned char* capture, LONG m_lImageWidth, LONG m_lImageHeight);
	
void SaveBitmapToFile( BYTE* pBitmapBits, LONG lWidth, LONG lHeight,WORD wBitsPerPixel, LPCTSTR lpszFileName )
{
    BITMAPINFOHEADER bmpInfoHeader = {0};
	BYTE  *hlp = (BYTE*)malloc(lWidth /** lHeight*/ * 4 * 2 /*2 Zeiulen */);
	DWORD *hlv;
	LONG   t, r, z, y, x;

	if (hlp == NULL) {
		MessageBox(NULL, L"Error allocate memory", L"Attention", MB_OK);
		return;
	}
    // Set the size
    bmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
    // Bit count
    bmpInfoHeader.biBitCount = wBitsPerPixel;
    // Use all colors
    bmpInfoHeader.biClrImportant = 0;
    // Use as many colors according to bits per pixel
    bmpInfoHeader.biClrUsed = 0;
    // Store as un Compressed
    bmpInfoHeader.biCompression = BI_RGB;
    // Set the height in pixels
    bmpInfoHeader.biHeight = lHeight;
    // Width of the Image in pixels
    bmpInfoHeader.biWidth = lWidth;
    // Default number of planes
    bmpInfoHeader.biPlanes = 1;
    // Calculate the image size in bytes
    bmpInfoHeader.biSizeImage = lWidth* lHeight * (wBitsPerPixel/8);

    BITMAPFILEHEADER bfh = {0};
    // This value should be values of BM letters i.e 0�4D42
    // 0�4D = M 0�42 = B storing in reverse order to match with endian
    bfh.bfType=0x4D42;
    /* or
    bfh.bfType = �B�+(�M� << 8);
    // <<8 used to shift �M� to end
    */
    // Offset to the RGBQUAD
    bfh.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);
    // Total size of image including size of headers
    bfh.bfSize = bfh.bfOffBits + bmpInfoHeader.biSizeImage;
    // Create the file in disk to write
    HANDLE hFile = CreateFile( lpszFileName,GENERIC_WRITE, 0,NULL,
                               CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,NULL);

    if( !hFile ) // return if error opening file
    {
        return;
    }

    DWORD dwWritten = 0;
    // Write the File header
    WriteFile( hFile, &bfh, sizeof(bfh), &dwWritten , NULL );
    // Write the bitmap info header
    WriteFile( hFile, &bmpInfoHeader, sizeof(bmpInfoHeader), &dwWritten, NULL );
    // Write the RGB Data

	RETAILMSG(1,(TEXT("lWidth = %d lHeight = %d\r\n"), lWidth, lHeight));

	_try {
	if ( camType != 0 ) { // monochrome cam
		for (r=0; r<lHeight; r++) {
			for (z=0; z< lWidth ; z++) {
				hlp[z*4+0] = ((BYTE*)pBitmapBits)[(lHeight-(r+1)) * lWidth + z];
				hlp[z*4+1] = ((BYTE*)pBitmapBits)[(lHeight-(r+1)) * lWidth + z];
				hlp[z*4+2] = ((BYTE*)pBitmapBits)[(lHeight-(r+1)) * lWidth + z];
			}
			WriteFile( hFile, hlp, lWidth*4, &dwWritten, NULL );
		}
	}
	else { // color cam
	
		for (t=0; t<0x100; t++) { 
			pat_r32[t] = ( ( ((DWORD)(((float)t)*mmr)) > 0xff ) ? 0xff: ((DWORD)(((float)t)*mmr)) ) << 16;
			pat_g32[t] = ( ( ((DWORD)(((float)t)*mmg)) > 0xff ) ? 0xff: ((DWORD)(((float)t)*mmg)) ) <<  8;
			pat_b32[t] = ( ( ((DWORD)(((float)t)*mmb)) > 0xff ) ? 0xff: ((DWORD)(((float)t)*mmb)) ) <<  0;
		}
RETAILMSG(1,(TEXT("correct color mmr=%d mmg=%d mmb=%d\r\n"), mmr, mmg, mmb));
RETAILMSG(1,(TEXT("ind = %d 0x%x 0x%x 0x%x \r\n"), 100, pat_r32[100], pat_g32[100], pat_b32[100]));
RETAILMSG(1,(TEXT("ind = %d 0x%x 0x%x 0x%x \r\n"), 200, pat_r32[200], pat_g32[200], pat_b32[200]));
		hlv = (DWORD*)hlp;
		for (y=lHeight-2; y>=0; y-=2)
		{
			for (x=0; x<lWidth; x+=2)
			{
			// dest[0][0] = src[0][0]=b src[1][1]=r src[0][1]=g
				hlv[ x ]               = pat_b32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x  ]] | pat_r32[((BYTE*)pBitmapBits)[(y+1)  * lWidth + x +1]] | pat_g32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x +1]];
				hlv[ x + 1 ]           = pat_b32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x  ]] | pat_r32[((BYTE*)pBitmapBits)[(y+1)  * lWidth + x +1]] | pat_g32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x +1]];
				hlv[ lWidth + x ]      = pat_b32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x  ]] | pat_r32[((BYTE*)pBitmapBits)[(y+1)  * lWidth + x +1]] | pat_g32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x +1]];
				hlv[ lWidth + x + 1 ]  = pat_b32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x  ]] | pat_r32[((BYTE*)pBitmapBits)[(y+1)  * lWidth + x +1]] | pat_g32[((BYTE*)pBitmapBits)[(y + 0) * lWidth + x +1]];
			}		
			WriteFile( hFile, hlv,  lWidth*8, &dwWritten, NULL );
		}
	}
	}  _except (EXCEPTION_EXECUTE_HANDLER) { RETAILMSG(1,(TEXT("except :: t = %d\r\n"), t)); }
    CloseHandle( hFile );
	free ( hlp );
}

void SaveRAWToFile( BYTE* pBitmapBits, LONG lWidth, LONG lHeight,WORD wBitsPerPixel, LPCTSTR lpszFileName )
{
	DWORD dwWritten = 0;

	RETAILMSG(1,(TEXT("SaveRAWToFile count = %d\r\n"), (lWidth * lHeight)));
	RETAILMSG(1,(TEXT("  use irfanview / raw / 8 bpp width = %d height = %d \r\n\r\n"), lWidth * lHeight ));

    HANDLE hFile = CreateFile( lpszFileName,GENERIC_WRITE, 0,NULL,
                             CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,NULL);

    if( !hFile ) // return if error opening file
    {
        return;
    }
	WriteFile( hFile, pBitmapBits, (lWidth * lHeight), &dwWritten, NULL );
    // Close the file handle
    CloseHandle( hFile );
}

void printfc(const wchar_t* printstring)
{
	SendMessage(wreg10, WM_SETTEXT, 5, (LPARAM)printstring); 
}

void StartImageProcessing(unsigned char* capture, LONG m_lImageWidth, LONG m_lImageHeight)
{
	char status[30];
	unsigned char* result;
	SaveBitmapToFile( (BYTE*)capture, m_lImageWidth, m_lImageHeight, 32, L"\\capture.bmp" );
	
	printfc(L"start circle detection...");
	
	RETAILMSG(1, (TEXT( "Run openCV \n")));
	RunOpenCV(InformationArray, result, m_lImageWidth, m_lImageHeight, &printfc);
	RETAILMSG(1, (TEXT( "End of openCV \n")));

	runStream = 1;	// start the stream: as soon as the event 0x1234 occurs, the stream will be started
}

CStreamTest::CStreamTest()
{
    m_hCamStream = INVALID_HANDLE_VALUE;
    m_hStreamMsgQueue = NULL;
    m_hASyncThreadHandle = NULL;
    m_hwndMain = NULL;
    m_hdcMain = NULL;
    m_pCSDataFormat = NULL;
    m_dwAllocationType = CSPROPERTY_BUFFER_CLIENT_LIMITED;
    m_dwDriverRequestedBufferCount = 3;
    m_pcsBufferNode = NULL;
    m_wBitCount = 0;
    m_lImageWidth = 0;
    m_lImageHeight = 0;
    m_csState = CSSTATE_STOP;
    memset(&m_rcClient, 0, sizeof(RECT));
    m_hMutex = NULL;
    m_pbmiBitmapDataFormat = NULL;
    m_nArtificialDelay = 0;
    m_nPictureNumber = 1;
    m_lPreviousPresentationTime = 0;
    m_lExpectedDuration = 0;
    m_dwExpectedDataSize = 0;
    m_fExpectZeroPresentationTime = FALSE;
    m_lASyncThreadCount = 0;
    m_sPinType = 0;
}

CStreamTest::~CStreamTest()
{
    Cleanup();
}

BOOL
CStreamTest::Cleanup()
{
    // keep state changes synchronous, set the state to stopped before cleanup,
    // make sure we're initialized before doing it though.
    if(m_hCamStream != INVALID_HANDLE_VALUE)
        SetState(CSSTATE_STOP);
    if(NULL != m_pCSDataFormat)
    {
        delete[] m_pCSDataFormat;
        m_pCSDataFormat = NULL;
    }
    ReleaseBuffers();
    SAFECLOSEFILEHANDLE(m_hCamStream);
    SAFECLOSEHANDLE(m_hStreamMsgQueue);
    SAFECLOSEHANDLE(m_hASyncThreadHandle);
    SAFECLOSEHANDLE(m_hMutex);
    if(m_hdcMain)
    {
        ReleaseDC(m_hwndMain, m_hdcMain);
    }

    if(m_hwndMain)
    {
        DestroyWindow(m_hwndMain);
        UnregisterClass(g_tszClassName, NULL);
    }
    m_hdcMain = NULL;

    return TRUE;
}

// create the message queue and give it to the driver
BOOL CStreamTest::CreateStream(ULONG ulPinId, TCHAR *szStreamName, SHORT sPinType)
{
    if(NULL == szStreamName)
        return FALSE;

    if(INVALID_HANDLE_VALUE != m_hCamStream)
        return TRUE;

    m_hCamStream = CreateFile(szStreamName,
                                GENERIC_READ|GENERIC_WRITE,
                                0,
                                NULL,
                                OPEN_EXISTING,
                                FILE_ATTRIBUTE_NORMAL,
                                NULL);

    if(INVALID_HANDLE_VALUE == m_hCamStream)
    {
        return FALSE;
    }

    m_hMutex = CreateMutex(NULL, FALSE, NULL);

    if(NULL == m_hMutex)
    {
        SAFECLOSEFILEHANDLE(m_hCamStream);
        return FALSE;
    }

    MSGQUEUEOPTIONS msgQOptions;

    msgQOptions.dwSize = sizeof(MSGQUEUEOPTIONS);
    msgQOptions.dwFlags = 0;
    msgQOptions.dwMaxMessages = 10;
    msgQOptions.cbMaxMessage = sizeof(CS_MSGQUEUE_BUFFER);
    msgQOptions.bReadAccess = TRUE;

    m_hStreamMsgQueue = CreateMsgQueue(NULL, &msgQOptions);
    if(NULL == m_hStreamMsgQueue)
    {
        WARN(TEXT("GetFrameAllocationInfoForStream : TestDeviceIOControl failed. Unable to get CSPROPERTY_STREAM_ALLOCATOR  "));
        SAFECLOSEFILEHANDLE(m_hCamStream);
        SAFECLOSEHANDLE(m_hMutex);
        return FALSE;
    }

    DWORD dwBytesReturned = 0;
    CSPROPERTY_STREAMEX_S csPropStreamEx;
    
    csPropStreamEx.CsPin.Property.Set = CSPROPSETID_StreamEx;
    csPropStreamEx.CsPin.Property.Id = CSPROPERTY_STREAMEX_INIT;
    csPropStreamEx.CsPin.PinId = ulPinId;
    csPropStreamEx.hMsgQueue = m_hStreamMsgQueue;
    
    if(FALSE == TestStreamDeviceIOControl(IOCTL_STREAM_INSTANTIATE,
                                                (LPVOID)&csPropStreamEx,
                                                sizeof(CSPROPERTY_STREAMEX_S),
                                                NULL,
                                                0,
                                                &dwBytesReturned,
                                                NULL))
    {
        SAFECLOSEFILEHANDLE(m_hCamStream);
        SAFECLOSEHANDLE(m_hMutex);
        CloseMsgQueue(m_hStreamMsgQueue);
        m_hStreamMsgQueue = NULL;
        return FALSE;
    }

    m_ulPinId = ulPinId;
    m_sPinType = sPinType;
    
    return TRUE;
}

BOOL CStreamTest::TestStreamDeviceIOControl(DWORD dwIoControlCode, 
                                                LPVOID lpInBuffer, 
                                                DWORD nInBufferSize, 
                                                LPVOID lpOutBuffer,  
                                                DWORD nOutBufferSize,  
                                                LPDWORD lpBytesReturned,  
                                                LPOVERLAPPED lpOverlapped)
{
    if(INVALID_HANDLE_VALUE == m_hCamStream)
        return FALSE;

    if(!DeviceIoControl(m_hCamStream, 
                        dwIoControlCode, 
                        lpInBuffer, 
                        nInBufferSize, 
                        lpOutBuffer,  
                        nOutBufferSize,  
                        lpBytesReturned,  
                        lpOverlapped))
    {
        return FALSE;
    }

    return TRUE;
}

// retrieve general property information from the driver, including frame and allocation information
BOOL CStreamTest::GetFrameAllocationInfoForStream()
{
    DWORD dwBytesReturned = 0;
    CSPROPERTY csProp;

    csProp.Set = CSPROPSETID_Connection;
    csProp.Id  = CSPROPERTY_CONNECTION_ALLOCATORFRAMING;
    csProp.Flags = CSPROPERTY_TYPE_GET;

    
    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_PROPERTY, 
                                            &csProp, 
                                            sizeof(csProp), 
                                            &m_CSAllocatorFraming, 
                                            sizeof(m_CSAllocatorFraming), 
                                            &dwBytesReturned,
                                            NULL))
    {
        FAIL(TEXT("GetFrameAllocationInfoForStream : TestDeviceIOControl failed. Unable to get CSPROPERTY_CONNECTION_ALLOCATORFRAMING  "));
        return FALSE;
    }

    m_dwDriverRequestedBufferCount = m_CSAllocatorFraming.Frames;
    m_dwAllocationType = m_CSAllocatorFraming.RequirementsFlags;

    return TRUE;
}

BOOL CStreamTest::FlushMsgQueue()
{
    CS_MSGQUEUE_BUFFER csMsgQBuffer;
    DWORD dwFlags = 0;
    DWORD dwBytesRead = 0;

    if(!m_hStreamMsgQueue)
        return FALSE;

    memset(&csMsgQBuffer, 0x0, sizeof(csMsgQBuffer));
    while(TRUE == ReadMsgQueue(m_hStreamMsgQueue, &csMsgQBuffer, sizeof(csMsgQBuffer), &dwBytesRead, 0, &dwFlags))
    {
        if(ERROR_TIMEOUT == GetLastError())
            break;
    }

    return TRUE;
}

BOOL CStreamTest::SetFormat(PCSDATAFORMAT pCSDataFormat, size_t DataFormatSize)
{
    DWORD dwBytesReturned = 0;
    DWORD dwFormatSize    = 0;
    CSPROPERTY csProp;

    // we're given DataFormatSize as the size of the buffer passed in, there's nothing much we can to about validating it
    // beyond making sure it's not 0.
    PREFAST_ASSUME(DataFormatSize);

    WaitForSingleObject(m_hMutex, INFINITE);
    if(NULL == pCSDataFormat)
    {
        FAIL(TEXT("SetFormat : NULL parameter passed."));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }

    if(0 == DataFormatSize)
    {
        FAIL(TEXT("SetFormat : invalid size passed."));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }

    csProp.Set = CSPROPSETID_Connection;
    csProp.Id = CSPROPERTY_CONNECTION_DATAFORMAT;
    csProp.Flags = CSPROPERTY_TYPE_SET;

    if(CSDATAFORMAT_SPECIFIER_VIDEOINFO == pCSDataFormat->Specifier)
    {
        dwFormatSize = sizeof(CS_DATARANGE_VIDEO);
    }
    else if(CSDATAFORMAT_SPECIFIER_VIDEOINFO2 == pCSDataFormat->Specifier)
    {
        dwFormatSize = sizeof(CS_DATARANGE_VIDEO2);
    }
    else
    {

        FAIL(TEXT("SetFormat : Unknown Format."));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }
    
    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_PROPERTY, 
                                            &csProp, 
                                            sizeof(CSPROPERTY), 
                                            pCSDataFormat, 
                                            dwFormatSize, 
                                            &dwBytesReturned,
                                            NULL))
    {
        FAIL(TEXT("SetFormat : TestDeviceIOControl failed. Unable to get CSPROPERTY_STREAM_ALLOCATOR  "));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }

    if(m_pCSDataFormat)
        delete[] m_pCSDataFormat;
    m_pCSDataFormat = NULL;

    // grab a copy of the pointer
    m_pCSDataFormat = reinterpret_cast< PCSDATAFORMAT > (new BYTE[ DataFormatSize ]);

    if(NULL == m_pCSDataFormat)
    {
        FAIL(TEXT("SetFormat : Unknown Format or out of memory."));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }
    memcpy(m_pCSDataFormat, pCSDataFormat, DataFormatSize);

    if(CSDATAFORMAT_SPECIFIER_VIDEOINFO == m_pCSDataFormat->Specifier)
    {
        PCS_DATAFORMAT_VIDEOINFOHEADER pCSDataFormatVidInfoHdr = reinterpret_cast<PCS_DATAFORMAT_VIDEOINFOHEADER>(m_pCSDataFormat);
        m_wBitCount = pCSDataFormatVidInfoHdr->VideoInfoHeader.bmiHeader.biBitCount;
        m_lImageWidth = pCSDataFormatVidInfoHdr->VideoInfoHeader.bmiHeader.biWidth;
        m_lImageHeight = abs(pCSDataFormatVidInfoHdr->VideoInfoHeader.bmiHeader.biHeight);

        // the video info header has a bmih followed by color table info, so it's basically a bitmapinfo
        m_pbmiBitmapDataFormat = (BITMAPINFO *) &(pCSDataFormatVidInfoHdr->VideoInfoHeader.bmiHeader);
    }
    else if(CSDATAFORMAT_SPECIFIER_VIDEOINFO2 == m_pCSDataFormat->Specifier)
    {
        PCS_DATAFORMAT_VIDEOINFOHEADER2 pCSDataFormatVidInfoHdr2 = reinterpret_cast<PCS_DATAFORMAT_VIDEOINFOHEADER2>(m_pCSDataFormat);
        m_wBitCount = pCSDataFormatVidInfoHdr2->VideoInfoHeader2.bmiHeader.biBitCount;
        m_lImageWidth = pCSDataFormatVidInfoHdr2->VideoInfoHeader2.bmiHeader.biWidth;
        m_lImageHeight = abs(pCSDataFormatVidInfoHdr2->VideoInfoHeader2.bmiHeader.biHeight);

		
        // the video info header has a bmih followed by color table info, so it's basically a bitmapinfo
        m_pbmiBitmapDataFormat = (BITMAPINFO *) &(pCSDataFormatVidInfoHdr2->VideoInfoHeader2.bmiHeader);
    }

    // we successfully changes the format, so flush the message queue so we don't process frames incorrectly.
    FlushMsgQueue();

    ReleaseMutex(m_hMutex);

    return TRUE;

}

BOOL CStreamTest::CreateCameraWindow(HWND hwnd, RECT rc)
{
    if(m_hwndMain)
        return TRUE;

    WNDCLASS wc;

    WaitForSingleObject(m_hMutex, INFINITE);

    memset(&wc, 0, sizeof(WNDCLASS));
    wc.lpfnWndProc = (WNDPROC)DefWindowProc;
    wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszClassName = g_tszClassName;

    // when there are multiple instances of this class, this register class may fail.
    RegisterClass(&wc);

    // create a window that fills the work area.
    // if we're given a parent window, then make this as a child, otherwise make it a primary
    m_hwndMain = CreateWindowEx(0, 
                                    g_tszClassName, 
                                    NULL, 
                                    (hwnd?WS_CHILD:0) | WS_BORDER, 
                                    rc.left, 
                                    rc.top, 
                                    rc.right - rc.left,
                                    rc.bottom - rc.top, 
                                    hwnd, 
                                    NULL, 
                                    NULL/*globalInst*/, 
                                    NULL);

    if(NULL == m_hwndMain)
    {
        FAIL(TEXT("CreateCameraWindow : CreateWindowEx failed."));
        UnregisterClass(g_tszClassName, NULL);
        ReleaseMutex(m_hMutex);
        return FALSE;
    }

    ShowWindow(m_hwndMain, SW_SHOWNORMAL);

    m_hdcMain = GetDC(m_hwndMain);

    if(NULL == m_hdcMain)
    {
        FAIL(TEXT("CreateCameraWindow : GetDC failed."));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }

    if(!GetClientRect(m_hwndMain, &m_rcClient))
    {
        FAIL(TEXT("CreateCameraWindow : GetClientRect failed."));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }

    ReleaseMutex(m_hMutex);

    return TRUE;
}

BOOL CStreamTest::SetState(CSSTATE csState)
{
    DWORD dwBytesReturned = 0;
    CSPROPERTY csProp;

    // keep state changes synchronous.
    WaitForSingleObject(m_hMutex, INFINITE);

    // if we're transitioning from the pause state, then reset time
    if(csState == CSSTATE_RUN && m_csState == CSSTATE_PAUSE)
    {
        m_lPreviousPresentationTime = 0;
        m_fExpectZeroPresentationTime = TRUE;
    }

    csProp.Set = CSPROPSETID_Connection;
    csProp.Id = CSPROPERTY_CONNECTION_STATE;
    csProp.Flags = CSPROPERTY_TYPE_SET;

    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_PROPERTY, 
                                            &csProp, 
                                            sizeof(CSPROPERTY), 
                                            &csState, 
                                            sizeof(CSSTATE), 
                                            &dwBytesReturned,
                                            NULL))
    {
        WARN(TEXT("SetState : TestDeviceIOControl failed. Unable to set CSPROPERTY_CONNECTION_STATE  "));
        ReleaseMutex(m_hMutex);
        return FALSE;
    }

//    if (m_sPinType != STREAM_STILL)
//    {
    if (csState == CSSTATE_STOP)
    {    
        if(FALSE == ReleaseBuffers())
        {
            SKIP(TEXT("CleanupStream : ReleaseBuffers failed"));
            return FALSE;
        }
    }

    // if we're going from stop to pause, we need to allocate the buffers
    if (m_csState == CSSTATE_STOP && csState == CSSTATE_PAUSE)
    {
        if(FALSE == AllocateBuffers())
        {
            SKIP(TEXT("SetState : AllocateBuffers failed"));
            return FALSE;
        }
        CreateAsyncThread();
    }
//    }

    // setting the state succeeded, save off the new current state so we know whether we're running, stopped, or paused.
    // The still pin should succeed this but not change state.
    if (!(csState == CSSTATE_RUN && m_sPinType == STREAM_STILL))
    {
        m_csState = csState;
    }

    ReleaseMutex(m_hMutex);

    // Wait for the io handler thread to finish off it's operations
    // MSGQUEUE_WAITTIME is the maximum amount of time it'll wait
    // on the message queue.
    Sleep(MSGQUEUE_WAITTIME);

    return TRUE;
}

CSSTATE CStreamTest::GetState()
{
    CSSTATE csState = m_csState;

    DWORD dwBytesReturned = 0;
    CSPROPERTY csProp;

    // keep state changes synchronous.
    WaitForSingleObject(m_hMutex, INFINITE);

    // if we're transitioning to the stop state, then we want to reset out
    // verification variables for the frame count & presentation times.
    if(csState == CSSTATE_STOP)
    {
        m_nPictureNumber = 1;
        m_lPreviousPresentationTime = 0;
        m_lExpectedDuration = 0;
        m_dwExpectedDataSize = 0;
    }

    csProp.Set = CSPROPSETID_Connection;
    csProp.Id = CSPROPERTY_CONNECTION_STATE;
    csProp.Flags = CSPROPERTY_TYPE_GET;

    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_PROPERTY, 
                                            &csProp, 
                                            sizeof(CSPROPERTY), 
                                            &csState, 
                                            sizeof(CSSTATE), 
                                            &dwBytesReturned,
                                            NULL))
    {
        FAIL(TEXT("SetState : TestDeviceIOControl failed. Unable to get CSPROPERTY_CONNECTION_STATE  "));
        ReleaseMutex(m_hMutex);
        return m_csState;
    }

    // setting the state succeeded, save off the new current state so we know whether we're running, stopped, or paused.
    if(m_csState != csState)
    {
        FAIL(TEXT("SetState : Expected pin state is not what was retrieved. "));
    }

    ReleaseMutex(m_hMutex);

    return csState;
}

BOOL CStreamTest::SetBufferCount(DWORD dwNewBufferCount)
{
    BOOL bReturnValue = TRUE;
    CSPROPERTY csProp;
    DWORD dwBytesReturned;
    CSALLOCATOR_FRAMING csFraming;

    memcpy(&csFraming, &m_CSAllocatorFraming, sizeof(CSALLOCATOR_FRAMING));
    csFraming.Frames = dwNewBufferCount;

    csProp.Set = CSPROPSETID_Connection;
    csProp.Id  = CSPROPERTY_CONNECTION_ALLOCATORFRAMING;
    csProp.Flags = CSPROPERTY_TYPE_SET;

     // get the requested number of buffers from the driver
    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_PROPERTY, 
                                                &csProp, 
                                                sizeof(CSPROPERTY), 
                                                &csFraming, 
                                                sizeof(CSALLOCATOR_FRAMING), 
                                                &dwBytesReturned,
                                                NULL))
    {
        FAIL(TEXT("SetBufferCount : TestDeviceIOControl failed. Unable to set allocator framing. "));
        bReturnValue = FALSE;
    }

    return bReturnValue;
}

BOOL CStreamTest::CreateAsyncThread()
{
    DWORD dwThreadID = 0;

    WaitForSingleObject(m_hMutex, INFINITE);

    m_hASyncThreadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ASyncIOThread, (LPVOID)this, 0, &dwThreadID);
    if(NULL == m_hASyncThreadHandle)
    {
        FAIL(TEXT("CreateAsyncThread : Could not create Status Thread"));
        return FALSE;
    }

    ReleaseMutex(m_hMutex);

    return TRUE;
}

BOOL CStreamTest::CleanupASyncThread()
{
    // make sure the async thread is stopped (it exits when it's stopped)
    SetState(CSSTATE_STOP);

    // now close out it's handle;
    SAFECLOSEHANDLE(m_hASyncThreadHandle);

    return TRUE;
}

BOOL CStreamTest::SetAritificalDelay(int nDelay)
{
    WaitForSingleObject(m_hMutex, INFINITE);
    m_nArtificialDelay = nDelay;
    ReleaseMutex(m_hMutex);
    return TRUE;
}

DWORD WINAPI CStreamTest::ASyncIOThread(LPVOID lpVoid)
{
    PCAMERASTREAMTEST pCamStreamTest = reinterpret_cast<PCAMERASTREAMTEST>(lpVoid);
    if(NULL != lpVoid)
        return pCamStreamTest->StreamIOHandler();
    else
        return THREAD_FAILURE;
}

GUID RGB565guid = GUID_16BPP565;
DWORD WINAPI CStreamTest::StreamIOHandler()
{
    DWORD dwTimeOut = 0;
    DWORD dwBytesRead = 0;
    DWORD dwFlags = 0;
    int iNoOfAttempts = 0;
    CS_MSGQUEUE_BUFFER csMsgQBuffer;
    
    
    if(NULL == m_hStreamMsgQueue)
    {
        WARN(TEXT("AsyncIOThread() : "));
        return THREAD_FAILURE;
    }

    if (InterlockedIncrement(&m_lASyncThreadCount) > 1)
    {
        InterlockedDecrement(&m_lASyncThreadCount);
        return 0;
    }


    do
    {
        // wait for something from the message queue.  If it doesn't come within the timeout
        // see if there was a state change.
        dwTimeOut = WaitForSingleObject (m_hStreamMsgQueue, (Singlechecked)?-1:400); // MSGQUEUE_WAITTIME); qaywsx
        // hold the mutex when using class state variables.
        WaitForSingleObject(m_hMutex, INFINITE);

        if(WAIT_TIMEOUT == dwTimeOut)
        {
            // if we're in the run state and the queue timed out, 
            // then output a warning that it did.  Still continue waiting though
            if(m_csState == CSSTATE_RUN)
            {
                // if we weren't told to stop, then just wait until we are.
                WARN(TEXT("ASyncIOThread : Timed Out"));
            }
            
            // if we stopped, just exit the io handler
            if(m_csState == CSSTATE_STOP)// && m_sPinType != STREAM_STILL)
            {

                // Make sure the buffer queue is completely flushed
                while(FALSE != ReadMsgQueue(m_hStreamMsgQueue, &csMsgQBuffer, sizeof(csMsgQBuffer), &dwBytesRead, MSGQUEUE_WAITTIME * 5, &dwFlags))
                {
                    DEBUGMSG(1,(TEXT("ASyncIOThread : Received buffer from queue while in stop state. Throwing out.")));
                }

                m_nPictureNumber = 1;
                m_lPreviousPresentationTime = 0;
                m_lExpectedDuration = 0;
                m_dwExpectedDataSize = 0;
                ReleaseMutex(m_hMutex);
                break;
            }

            ReleaseMutex(m_hMutex);
            // whether we're paused or run, continue waiting on the queue.
            continue;
        }

        // Read Msg.
        memset(&csMsgQBuffer, 0x0, sizeof(csMsgQBuffer));
        if(FALSE == ReadMsgQueue(m_hStreamMsgQueue, &csMsgQBuffer, sizeof(csMsgQBuffer), &dwBytesRead, MSGQUEUE_WAITTIME, &dwFlags))
        {
            ERRFAIL(TEXT("ASyncIOThread : ReadMsgQueue returned FALSE "));
            ReleaseMutex(m_hMutex);
            break;
        }

        PCS_MSGQUEUE_HEADER pCSMsgQHeader = NULL;
        PCS_STREAM_DESCRIPTOR pStreamDescriptor = NULL;
        PCSSTREAM_HEADER pCsStreamHeader = NULL;

        pCSMsgQHeader = &csMsgQBuffer.CsMsgQueueHeader;
        if(NULL == pCSMsgQHeader)
        {
            ERRFAIL(TEXT("ASyncIOThread : MessageQueue Header is NULL"));
            ReleaseMutex(m_hMutex);
            break;
        }

        if(FLAG_MSGQ_FRAME_BUFFER & ~pCSMsgQHeader->Flags)
        {
            WARN(TEXT("ASyncIOThread : The buffer retrieved from Queue is of custom type for driver"));
        }

        pStreamDescriptor = csMsgQBuffer.pStreamDescriptor;
        
        if(NULL == pStreamDescriptor)
        {
            ERRFAIL(TEXT("ASyncIOThread : Stream Descriptor is NULL"));
            ReleaseMutex(m_hMutex);
            break;
        }
        
        pCsStreamHeader = &pStreamDescriptor->CsStreamHeader;
        if(NULL == pCsStreamHeader)
        {
            ERRFAIL(TEXT("ASyncIOThread : Stream Header is NULL"));
            ReleaseMutex(m_hMutex);
            break;
        }
        // DEBUGMSG(1,(TEXT("Frame Id%d "), pStreamDescriptor->CsFrameInfo.PictureNumber));

		if (1||halbe) {
			if (halbecnt > 400) {
				if ( oldto ) {
					RETAILMSG(1,(TEXT("bilder / sek: %d\r\n "), ( (halbecnt*1000) / (GetTickCount()-oldto) ) ));
					halbecnt = 0;
				}
				oldto = GetTickCount();
			}
			halbecnt++;
		}

		if ( ( m_sPinType == STREAM_CAPTURE) && ( pCsStreamHeader->DataUsed == 752 * 480 ) ) { 

			if(m_csState == CSSTATE_STOP)
			{
				// RETAILMSG(1,(TEXT("stop STREAM_CAPTURE %d!!\r\n"), GetTickCount() ));
			} else {
//				RETAILMSG(1,(TEXT("IST IN DER APPL ... STREAM_CAPTURE\r\n")));
					memcpy(capture, pCsStreamHeader->Data, pCsStreamHeader->DataUsed);
					//  RETAILMSG(1,(TEXT("CAPTURE COPY done\r\n")));
			}
		}
			

	if ( ( m_sPinType == STREAM_STILL ) && ( pCsStreamHeader->DataUsed == 752 * 480 ) ) {		
			if(m_csState == CSSTATE_STOP)
			{
				// RETAILMSG(1,(TEXT("stop STREAM_STILL !! %d\r\n"), GetTickCount() ));
			}
			else {
			RETAILMSG(1,(TEXT("saveraw = %d w=%d h=%d\r\n"), saveraw, m_lImageWidth, m_lImageHeight));			
			

			// save picture
				if ( !saveraw )
				{
					SaveBitmapToFile( (BYTE*)capture, m_lImageWidth, m_lImageHeight, 32, L"\\capture.bmp" );
				}
				else 
				//SaveRAWToFile( (BYTE*)capture, m_lImageWidth, m_lImageHeight, 32, L"\\capture.raw" );
				/* instead of saving a file, this function is used for starting image processing */
				{
				StartImageProcessing(capture, m_lImageWidth, m_lImageHeight);
				saveraw = 0;
				}
			}
		}

        if(m_csState == CSSTATE_STOP)// && m_sPinType != STREAM_STILL)
        {
            DEBUGMSG(1,(TEXT("ASyncIOThread : Received buffer from queue while in stop state. Throwing out.")));
            ++m_nPictureNumber;
            ReleaseMutex(m_hMutex);
            continue;
        }

        if(m_nPictureNumber == 1)
        {
            // make sure the retrieved picture number is 0
            if(pStreamDescriptor->CsFrameInfo.PictureNumber != 1)
            {
                ERRFAIL(TEXT("ASyncIOThread : Initial picture number isn't 1."));
                DEBUGMSG(1,(TEXT("Initial picture number: %d"), (int)(pStreamDescriptor->CsFrameInfo.PictureNumber)));
            }

            // make sure the presentation time is 0
            if(pStreamDescriptor->CsStreamHeader.PresentationTime.Time != 0 && m_sPinType != STREAM_STILL)
            {
                ERRFAIL(TEXT("ASyncIOThread : Initial presentation time isn't 0."));
                DEBUGMSG(1,(TEXT("Initial presentation time: %d"), pStreamDescriptor->CsStreamHeader.PresentationTime.Time));
            }

            m_lExpectedDuration = pStreamDescriptor->CsStreamHeader.Duration;
            m_dwExpectedDataSize = pStreamDescriptor->CsStreamHeader.DataUsed;
            m_fExpectZeroPresentationTime = FALSE;
        }
        else
        {
            // verify that the picture number we think we're on is the same as we're actually on
            if(m_nPictureNumber != pStreamDescriptor->CsFrameInfo.PictureNumber)
            {
                ERRFAIL(TEXT("ASyncIOThread : Picture number didn't increase between frames."));
                DEBUGMSG(1,(TEXT("Expected picture number is %d, actual picture number is %d (resetting expected)"),
                    m_nPictureNumber,
                    pStreamDescriptor->CsFrameInfo.PictureNumber));
                m_nPictureNumber = (int)pStreamDescriptor->CsFrameInfo.PictureNumber;
            }

            // verify the new presentation time is later than the previous
            if(m_sPinType == STREAM_STILL)
            {
                // Don't verify the presentation time.
            }
            else if(!m_fExpectZeroPresentationTime && m_lPreviousPresentationTime >= pStreamDescriptor->CsStreamHeader.PresentationTime.Time)
            {
                ERRFAIL(TEXT("ASyncIOThread : Previous presentation time is before the current presentation time."));
                DEBUGMSG(1,(TEXT("ASyncIOThread : Previous presentation time is 0x%08x, new presentation time is 0x%08x"), m_lPreviousPresentationTime, pStreamDescriptor->CsStreamHeader.PresentationTime.Time));
            }
            else if (m_fExpectZeroPresentationTime)
            {
                if (pStreamDescriptor->CsStreamHeader.PresentationTime.Time != 0)
                {
                    ERRFAIL(TEXT("ASyncIOThread : Expected PresentationTime == 0, found nonzero presentation time"));
                    DEBUGMSG(1,(TEXT("ASyncIOThread : Expected presentation time is 0, actual presentation time is 0x%08x"), pStreamDescriptor->CsStreamHeader.PresentationTime.Time));
                }
                m_fExpectZeroPresentationTime = FALSE;
            }
        }

        m_nPictureNumber++;
        m_lPreviousPresentationTime = pStreamDescriptor->CsStreamHeader.PresentationTime.Time;

        // verify the duration is what we'd expect
        if(m_lExpectedDuration != pStreamDescriptor->CsStreamHeader.Duration)
        {
            ERRFAIL(TEXT("ASyncIOThread : Actual duration doesn't match the expected duration."));
        }

        // verify the data size is what we'd expect
        if(m_dwExpectedDataSize != pStreamDescriptor->CsStreamHeader.DataUsed)
        {
            ERRFAIL(TEXT("ASyncIOThread : actual data used doesn't match the expected data used."));
        }
      


	  
//        GUID RGB565guid = GUID_16BPP565;
#if 1 // qaywsx hub's
//        if(RGB565guid == m_pCSDataFormat->SubFormat)

		if ( ( pCsStreamHeader->DataUsed == 376 * 240 * 2) &&  ( m_sPinType == STREAM_PREVIEW) ) 
        {
	
			if ( !resize_window) {
				if(FALSE ==  SetDIBitsToDevice(m_hdcMain,
                                                        m_rcClient.left,
                                                        m_rcClient.top,
                                                        m_lImageWidth,
                                                        m_lImageHeight,
														0,
                                                        0,
                                                        0,
                                                        m_lImageHeight,
                                                        pCsStreamHeader->Data, 
                                                        m_pbmiBitmapDataFormat,  
                                                        DIB_RGB_COLORS) )
								{
									ERRFAIL(TEXT("ASyncIOThread : StretchDIBitsFailed") );
									ReleaseMutex(m_hMutex);
									break;
								}	
			} else {
														
            if(FALSE == StretchDIBits(m_hdcMain,
                                                        m_rcClient.left,
                                                        m_rcClient.top,
                                                        (m_rcClient.right - m_rcClient.left),
                                                        (m_rcClient.bottom - m_rcClient.top), 
                                                        0,
                                                        0,
                                                        m_lImageWidth,
                                                        m_lImageHeight,
                                                        pCsStreamHeader->Data, 
                                                        m_pbmiBitmapDataFormat,  
                                                        DIB_RGB_COLORS, 
                                                        SRCCOPY)) 
								{
									ERRFAIL(TEXT("ASyncIOThread : StretchDIBitsFailed") );
									ReleaseMutex(m_hMutex);
									break;
								}	
			}
        }


#endif
        // insert an artificial delay if requested
		if(m_nArtificialDelay) {
            Sleep(m_nArtificialDelay);
		}

// count_only:
// done:        
        if(m_csState != CSSTATE_STOP)
        {
            EnqueueBuffer(pStreamDescriptor);
        }

        ReleaseMutex(m_hMutex);
    } while (1/*We do not get Last Buffer */);

    InterlockedDecrement(&m_lASyncThreadCount);


    return THREAD_SUCCESS;
}

DWORD CStreamTest::GetDriverBufferCount()
{
    DWORD dwRequestedBuffer;

    WaitForSingleObject(m_hMutex, INFINITE);
    dwRequestedBuffer = m_dwDriverRequestedBufferCount;
    ReleaseMutex(m_hMutex);

    return dwRequestedBuffer;
}

DWORD CStreamTest::GetAllocationType()
{
    DWORD dwAllocationType;

    WaitForSingleObject(m_hMutex, INFINITE);
    dwAllocationType = m_dwAllocationType;
    ReleaseMutex(m_hMutex);

    return dwAllocationType;
}

BOOL CStreamTest::AllocateBuffers()
{
    BOOL bReturnValue = TRUE;
    ULONG ulBufferIndex;

    WaitForSingleObject(m_hMutex, INFINITE);

    if(m_dwAllocationType != CSPROPERTY_BUFFER_CLIENT_LIMITED &&
        m_dwAllocationType != CSPROPERTY_BUFFER_CLIENT_UNLIMITED &&
        m_dwAllocationType != CSPROPERTY_BUFFER_DRIVER)
    {
        ERRFAIL(TEXT("AllocateBuffers : Invalid buffering mode."));
        bReturnValue = FALSE;
        goto cleanup;
    }

    PSTREAM_BUFFERNODE pcsCurrentBufferNode = m_pcsBufferNode;

    // get to the end of the list.
    if(pcsCurrentBufferNode)
    {
        while(pcsCurrentBufferNode->pNext)
            pcsCurrentBufferNode = pcsCurrentBufferNode->pNext;
    }

    for(ulBufferIndex = 0; ulBufferIndex < m_dwDriverRequestedBufferCount; ulBufferIndex++)
    {
        // allocate our buffer node
        // if our current node is non-zero, then we're in the middle of the list.
        // allocate a new node, 
        if(pcsCurrentBufferNode)
        {
            pcsCurrentBufferNode->pNext = new(STREAM_BUFFERNODE);
            pcsCurrentBufferNode = pcsCurrentBufferNode->pNext;
        }
        else // we're starting a new linked list, so set the current node, and set the beginning pointer
        {
            pcsCurrentBufferNode = new(STREAM_BUFFERNODE);
            m_pcsBufferNode = pcsCurrentBufferNode;
        }

        if(!pcsCurrentBufferNode)
        {
            ERRFAIL(TEXT("AllocateBuffers : buffer node allocation failed."));
            bReturnValue = FALSE;
            goto cleanup;
        }

        // clear out the node
        memset(pcsCurrentBufferNode, 0, sizeof(STREAM_BUFFERNODE));

        // allocate the stream descriptor
        pcsCurrentBufferNode->pCsStreamDesc = new(CS_STREAM_DESCRIPTOR);
        if(!pcsCurrentBufferNode->pCsStreamDesc)
        {
            ERRFAIL(TEXT("AllocateBuffers : stream descriptor allocation failed."));
            bReturnValue = FALSE;
            goto cleanup;
        }

        memset(pcsCurrentBufferNode->pCsStreamDesc, 0, sizeof(CS_STREAM_DESCRIPTOR));

        // if software allocate frame
        if(m_dwAllocationType == CSPROPERTY_BUFFER_CLIENT_LIMITED || m_dwAllocationType == CSPROPERTY_BUFFER_CLIENT_UNLIMITED)
        {
            pcsCurrentBufferNode->pCsStreamDesc->CsStreamHeader.Data = (PVOID) new(BYTE[m_pCSDataFormat->SampleSize]);

            if(!pcsCurrentBufferNode->pCsStreamDesc->CsStreamHeader.Data)
            {
                ERRFAIL(TEXT("AllocateBuffers : buffer frame allocation failed."));
                bReturnValue = FALSE;
                goto cleanup;
            }
        }

        // pass the frame to the driver
        if(FALSE == RegisterBuffer(pcsCurrentBufferNode->pCsStreamDesc))
        {
            ERRFAIL(TEXT("AllocateBuffers : Failed to register the buffer with the driver."));
            bReturnValue = FALSE;
            goto cleanup;
        }

        // pass the frame to the driver
        if(FALSE == EnqueueBuffer(pcsCurrentBufferNode->pCsStreamDesc))
        {
            ERRFAIL(TEXT("AllocateBuffers : Failed to queue the buffer up with the driver."));
            bReturnValue = FALSE;
            goto cleanup;
        }
    }
cleanup:
    ReleaseMutex(m_hMutex);

    return bReturnValue;
}

BOOL CStreamTest::ReleaseBuffers()
{
    // free the data structures before closing the file handles, as we need to
    // make calls into the driver.
    // repeat until the linked list is empty.
    while(m_pcsBufferNode)
    {
        // backup the current node
        PSTREAM_BUFFERNODE csTempBufferNode = m_pcsBufferNode;

        // set the current node to the next node, for the next iteration
        m_pcsBufferNode = m_pcsBufferNode->pNext;

        // if the stream descriptor was allocated
        if(csTempBufferNode->pCsStreamDesc)
        {
            // now release the temp buffer's pointers.
            ReleaseBuffer(csTempBufferNode->pCsStreamDesc);

            // free the data pointer ifwe're in software mode
            if(m_dwAllocationType == CSPROPERTY_BUFFER_CLIENT_LIMITED || m_dwAllocationType == CSPROPERTY_BUFFER_CLIENT_UNLIMITED)
            {
                delete csTempBufferNode->pCsStreamDesc->CsStreamHeader.Data;
                csTempBufferNode->pCsStreamDesc->CsStreamHeader.Data = NULL;
            }

            // free the stream descrioptor allocated
            delete csTempBufferNode->pCsStreamDesc;
            csTempBufferNode->pCsStreamDesc = NULL;
        }

        // finally, delete the actual node
        delete csTempBufferNode;
        csTempBufferNode = NULL;
    };

    return TRUE;
}

BOOL CStreamTest::EnqueueBuffer(PCS_STREAM_DESCRIPTOR pStreamDescriptor)
{
    // return the buffer back to the driver
    DWORD dwBytesReturned = 0;
    BOOL bReturnValue = TRUE;
    CSBUFFER_INFO BufferInfo;
    BufferInfo.dwCommand = CS_ENQUEUE;
    BufferInfo.pStreamDescriptor = pStreamDescriptor;

    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_BUFFERS, 
                                            &BufferInfo, 
                                            sizeof(CSBUFFER_INFO), 
                                            pStreamDescriptor, 
                                            sizeof(CS_STREAM_DESCRIPTOR), 
                                            &dwBytesReturned,
                                            NULL))
    {
        ERRFAIL(TEXT("EnqueueBuffer : Call to enqueue the buffer failed."));
        bReturnValue = FALSE;
    }

    return bReturnValue;
}

BOOL CStreamTest::RegisterBuffer(PCS_STREAM_DESCRIPTOR pStreamDescriptor)
{
    // return the buffer back to the driver
    DWORD dwBytesReturned = 0;
    BOOL bReturnValue = TRUE;
    CSBUFFER_INFO BufferInfo;
    BufferInfo.dwCommand = CS_ALLOCATE;
    BufferInfo.pStreamDescriptor = pStreamDescriptor;

    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_BUFFERS, 
                                            &BufferInfo, 
                                            sizeof(CSBUFFER_INFO), 
                                            pStreamDescriptor, 
                                            sizeof(CS_STREAM_DESCRIPTOR), 
                                            &dwBytesReturned,
                                            NULL))
    {
        ERRFAIL(TEXT("RegisterBuffer : Call to allocate the buffer failed."));
        bReturnValue = FALSE;
    }

    return bReturnValue;
}


// now that we've finished with the buffer, give it back to the driver for future use.
BOOL CStreamTest::ReleaseBuffer(PCS_STREAM_DESCRIPTOR    pStreamDescriptor)
{
    BOOL bReturnValue = TRUE;

    // free the buffer (IOCTL_CS_BUFFERS, CS_DEALLOCATE)
    DWORD dwBytesReturned = 0;
    CSBUFFER_INFO BufferInfo;
    BufferInfo.dwCommand = CS_DEALLOCATE;
    BufferInfo.pStreamDescriptor = pStreamDescriptor;

    if(FALSE == TestStreamDeviceIOControl (IOCTL_CS_BUFFERS, 
                                            &BufferInfo, 
                                            sizeof(CSBUFFER_INFO), 
                                            pStreamDescriptor, 
                                            sizeof(CS_STREAM_DESCRIPTOR), 
                                            &dwBytesReturned,
                                            NULL))
    {
        ERRFAIL(TEXT("ReleaseBuffer : Call to release the buffer failed."));
        bReturnValue = FALSE;
    }

    return bReturnValue;
}

BOOL CStreamTest::GetNumberOfFramesProcessed()
{
    int nPictureCount = 0;
    // keep state changes synchronous.
    WaitForSingleObject(m_hMutex, INFINITE);
    nPictureCount = m_nPictureNumber;
    ReleaseMutex(m_hMutex);

    return nPictureCount;
}

