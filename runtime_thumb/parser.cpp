/*!
	Parse das ueberegebene Registerfile & setzte 
	die Werte via I2C
*/

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <commdlg.h>
#include <stdio.h>
#include <string.h>

extern DWORD camType;	// 0 = color, 1 = mono
unsigned int nline = 0;
void WriteI2C ( unsigned char reg, unsigned short val, char mode );

void ParseLine ( char * line ) {
	USHORT	t, r;
	USHORT  val;
	USHORT	komma = 0;
	UCHAR	reg;
	UCHAR	addr;
	UCHAR	mode;
	BOOL	hexValue = TRUE;
	BOOL	showError = TRUE;
	
	nline ++;
	for ( t=0, r=0; t<MAX_PATH; t++ )
	{
		if ( line[t] == '#' ) {
			line[t] = 0;
			break;
		}

		if ( line[t] == ',' ) {
			komma++;

			if ( komma == 2 ) { // naechster Wert ist der Registerwert, der hex oder dezimal sein kann
				if ( ( line[t+1] != '0' ) || ( line[t+2] != 'x' ) )
					hexValue = FALSE;
			}

		}
		
		if ( line[t] != ' ' )
			line[r++] = line[t];

		if (!line[t])
			break;
	}

	if ( komma == 2 ) {
		if ( hexValue ) 
			sscanf(line, "%x,%x,%x\r\n", &addr, &reg, &val);
		else
			sscanf(line, "%x,%x,%d\r\n", &addr, &reg, &val);
// RETAILMSG(1,(TEXT("komma = %d line = %S | 0x%x 0x%x 0x%x (= %d) | hex = 0x%x\r\n"), komma, line, addr, reg, val, val, hexValue));
		if (0x48 != addr)
			showError = TRUE;
		else {
			WriteI2C ( reg, val, 's' );
			showError = FALSE;
		}
		
	}
	if ( komma == 3 ) {
		if ( hexValue ) 
			sscanf(line, "%x,%x,%x,%c", &addr, &reg, &val, &mode);
		else
			sscanf(line, "%x,%x,%d,%c", &addr, &reg, &val, &mode);
// RETAILMSG(1,(TEXT("komma = %d line = %S | 0x%x 0x%x 0x%x (= %d) %c | hex = 0x%x\r\n"), komma, line, addr, reg, val, val, mode, hexValue));			
		if (0x48 != addr)
			showError = TRUE;
		else {
			WriteI2C ( reg, val, mode );
			showError = FALSE;
		}
	}
	if ( komma == 0 )
		showError = FALSE;
		
	// RETAILMSG(1,(TEXT("addr= 0x%x komma = %d line (%d) = %S\r\n"), addr, komma, nline, line));
	if ( showError || (addr > 0xff) ) 
	{
		USHORT hint[1024];
//changed wsprintf(hint, L"Konfigurationsdatei fehlerhaft ! Zeile: %d (%S)", nline, line);
		wsprintf(LPWSTR(hint), L"Konfigurationsdatei fehlerhaft ! Zeile: %d (%S)", nline, line);
		// showError = FALSE;
//changed		MessageBox(NULL, hint, L"Fehler", MB_OK);
		MessageBox(NULL, LPCWSTR(hint), L"Fehler", MB_OK);
	}
	
}

void ParseFile ( TCHAR *filename ) 
{
	FILE 	*f;
	char	line[MAX_PATH];
	RETAILMSG(1,(TEXT("	ParseFile ( %s ) \r\n"), filename )); 
	nline = 0;
	if ( !( f = _wfopen ( filename, _T("rt") ) ))
		return;
	while (!feof(f) ) {
		if ( NULL == fgets( line, MAX_PATH-1, f) )
			break;

		ParseLine ( line ) ;
	}
		
	fclose(f);
}



BOOL ReadRegistryData( USHORT *regpath ) 
{
    LONG  regError;
    HKEY  hKey;
    DWORD dwDataSize;
	DWORD useReg;
	
    // Open the registry key
    regError = RegOpenKeyEx(HKEY_LOCAL_MACHINE,TEXT("Drivers\\BuiltIn\\Camera"),0,KEY_ALL_ACCESS,&hKey);
    if (regError != ERROR_SUCCESS)
    {
        RETAILMSG(1,(TEXT("Failed opening HKEY_CURRENT_USER\\Drivers\\BuiltIn\\Camera -> set type to 0\r\n")));
		regpath[0] = 0;
        return (FALSE);
    }
	dwDataSize = MAX_PATH-1;
    regError   = RegQueryValueEx(hKey,L"RegFile", NULL, NULL,(LPBYTE)regpath,&dwDataSize);
    if (regError != ERROR_SUCCESS)
    {
		regpath[0] = 0;
	}
	
    dwDataSize = sizeof(DWORD);
    regError   = RegQueryValueEx(hKey,L"NoRegFile", NULL, NULL,(LPBYTE)&useReg,&dwDataSize);
    if (regError == ERROR_SUCCESS)
    {
		RETAILMSG(1,(TEXT("	regError == ERROR_SUCCESS, useReg = %d\r\n"), useReg));
		if ( ! useReg ) 
			regpath[0] = 0;
	} else useReg = 1;
	
	RETAILMSG(1,(TEXT("ReadRegistryData Read from %s\r\n"), regpath));
	RETAILMSG(1,(TEXT("					useReg =  %d\r\n"), useReg));
	RegCloseKey(hKey);
	return (TRUE);
}

void initVM010 ( void ) 
{
	USHORT regpath[MAX_PATH];
#ifdef PHYCARD
	RETAILMSG(1,(TEXT("PhyCard detected ... use LVDS\r\n")));
	WriteI2C( 0xb3, 0x0000, 's' );
	WriteI2C( 0xb1, 0x0000, 's' );
	WriteI2C( 0x0c, 0x0001, 's' );
	WriteI2C( 0x0c, 0x0000, 's' );
#endif
    switch (camType)
    {
     case 0:
           RETAILMSG(1,(TEXT("Cam is a color cam ...\r\n")));
		   WriteI2C( 0x0f, 0x0103, 's' );
           break;
                 
     case 1:
			RETAILMSG(1,(TEXT("Cam is a monochrome cam ...\r\n")));	
			WriteI2C( 0x0f, 0x0101, 's' );
           break;
    }   

	WriteI2C( 0x07, 0x0388, 's' );
	WriteI2C( 0x04, 0x00f0, 's' );
	WriteI2C( 0x03, 0x0140, 's' );
	WriteI2C( 0x02, 0x0004, 's' );
	WriteI2C( 0x01, 0x0001, 's' );
	
	WriteI2C( 0x04, 0x02f0, 's' );
	WriteI2C( 0x03, 0x01e0, 's' );
	
	WriteI2C( 0x05, 0x5e, 's'); // 0x0276, 's' );

	// WriteI2C( 0x06, 0x2d, 's' ); // 0x0401, 's' );
	WriteI2C( 0x06, 0x6d, 's' ); // 0x0401, 's' );

	ReadRegistryData(regpath);
//changed ParseFile(regpath);
	ParseFile((TCHAR*)(regpath));
}

