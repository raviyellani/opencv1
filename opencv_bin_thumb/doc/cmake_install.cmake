# Install script for directory: C:/opencv/opencv_source/doc

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "C:/opencv/opencv_bin_thumb")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "main")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc" TYPE FILE FILES
    "C:/opencv/opencv_source/doc/ChangeLog.htm"
    "C:/opencv/opencv_source/doc/haartraining.htm"
    "C:/opencv/opencv_source/doc/index.htm"
    "C:/opencv/opencv_source/doc/CMakeLists.txt"
    "C:/opencv/opencv_source/doc/latex_readme.txt"
    "C:/opencv/opencv_source/doc/license.txt"
    "C:/opencv/opencv_source/doc/packaging.txt"
    "C:/opencv/opencv_source/doc/python.txt"
    "C:/opencv/opencv_source/doc/opencv.jpg"
    "C:/opencv/opencv_source/doc/opencv-logo.png"
    "C:/opencv/opencv_source/doc/opencv-logo2.png"
    "C:/opencv/opencv_source/doc/opencv.pdf"
    "C:/opencv/opencv_source/doc/pattern.pdf"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "main")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "main")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/papers" TYPE FILE FILES
    "C:/opencv/opencv_source/doc/papers/algo_tracking.pdf"
    "C:/opencv/opencv_source/doc/papers/camshift.pdf"
    "C:/opencv/opencv_source/doc/papers/avbpa99.ps"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "main")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "main")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/vidsurv" TYPE FILE FILES
    "C:/opencv/opencv_source/doc/vidsurv/Blob_Tracking_Modules.doc"
    "C:/opencv/opencv_source/doc/vidsurv/Blob_Tracking_Tests.doc"
    "C:/opencv/opencv_source/doc/vidsurv/TestSeq.doc"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "main")

