# CMake generated Testfile for 
# Source directory: C:/opencv/opencv_source
# Build directory: C:/opencv/opencv_bin_win32
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(src)
SUBDIRS(apps)
SUBDIRS(doc)
SUBDIRS(data)
SUBDIRS(tests)
SUBDIRS(interfaces)
SUBDIRS(3rdparty)
